sudo systemctl stop gunicorn.service
sudo systemctl stop nginx.service
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python manage.py migrate
python manage.py collectstatic --noinput
sudo systemctl start gunicorn.service
sudo systemctl start nginx.service